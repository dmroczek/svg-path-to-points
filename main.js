$(() => {
    console.log("Hello!");

    const canvas = document.getElementById('gc');
    const ctx = canvas.getContext("2d");
    let trackName = 'track.svg';

    var img = new Image();
    img.src = trackName;
    img.onload = function() {
        ctx.globalCompositeOperation = 'destination-over';
        // ctx.drawImage(img, 0, 0);
    };

    let svg = $.get({
        url: trackName,
        async: false
    }).responseXML;

    const PRECISION = 50;

    $(svg).find('path.track').toArray().forEach(path => {
        //let points = pathToPolygon(path, PRECISION);
        //let points = pathToPoints(path, PRECISION);
        //let points = pathToPointsAdaptive(path, 1.003);
        let points = pathToPointsAdaptiveSegments(path, 1.005);
        console.dir(points);

        if (!points || !points.length) {
            return;
        }
        // points.filter(p => !!p || p.x !== NaN || p.y !== NaN);
        ctx.beginPath();
        let lastPoint = points[points.length - 1];
        ctx.moveTo(lastPoint.x, lastPoint.y);
        ctx.fillStyle = 'red';
        points.forEach(p => {
            ctx.lineTo(p.x, p.y);
            ctx.stroke();
            ctx.save();
            ctx.beginPath();
            ctx.arc(p.x, p.y, 2, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.restore();

        });
    });

});

function pathToPointsAdaptiveSegments(path, tolerance, minDistance) {
    if (!path) {
        return [];
    }

    tolerance = tolerance || 1.005;
    minDistance = minDistance || 5;

    let segInfo = getSegmentData(path);
    console.dir(segInfo);

    let getDist = function (p1, p2) {
        return Math.sqrt(
            (Math.pow(p1.x - p2.x, 2)) +
            (Math.pow(p1.y - p2.y, 2))
        );
    };

    let interpolate = function (start, end) {
        let realDist = Math.abs(end - start);
        let p1 = path.getPointAtLength(start);
        let p2 = path.getPointAtLength(end);
        let fakeDist = getDist(p1, p2);

        if (realDist === 0) {
            return [];
        }

        if (fakeDist !== 0 && realDist / fakeDist <= tolerance) {
            return [];
        }

        if (fakeDist !== 0 && fakeDist <= minDistance) {
            return [];
        }

        let half = realDist / 2 + start;

        return [].concat(
            interpolate(start, half),
            path.getPointAtLength(half),
            interpolate(half, end)
        )
    };

    let output = [];
    for (let i = 1; i < segInfo.length; i += 1) {
        output.push(path.getPointAtLength(segInfo[i - 1].length));
        output.push(interpolate(segInfo[i - 1].length, segInfo[i].length));
    }

    output.push(path.getPointAtLength(segInfo[segInfo.length - 1].length));

    return [].concat(...output);
}

function pathToPointsAdaptive(path, tolerance, minDistance) {
    if (!path) {
        return [];
    }

    tolerance = tolerance || 1.002;
    minDistance = minDistance || 5;

    let getDist = function (p1, p2) {
        return Math.sqrt(
            (Math.pow(p1.x - p2.x, 2)) +
            (Math.pow(p1.y - p2.y, 2))
        );
    };

    let interpolate = function (start, end) {
        let realDist = Math.abs(end - start);
        let p1 = path.getPointAtLength(start);
        let p2 = path.getPointAtLength(end);
        let fakeDist = getDist(p1, p2);

        if (fakeDist !== 0 && realDist / fakeDist <= tolerance) {
            return [];
        }

        if (fakeDist !== 0 && fakeDist <= minDistance) {
            return [];
        }

        let half = realDist / 2 + start;

        return [].concat(
            interpolate(start, half),
            path.getPointAtLength(half),
            interpolate(half, end)
        )
    };

    return [].concat(
        path.getPointAtLength(0),
        interpolate(0, path.getTotalLength()),
        path.getPointAtLength(path.getTotalLength())
    );
}

function pathToPoints(path, distance) {
    if (!distance) {
        distance = 100;
    }

    let output = [];

    let segData = getSegmentData(path);

    let interpolate = function(length, length2) {
        let segmentLength = length2 - length;
        if (segmentLength < distance) {
            return [];
        }

        let howMany = Math.ceil(segmentLength / distance);
        let step = segmentLength / howMany;
        let points = [];

        for (let i = 0; i < howMany; i += 1) {
            length += step;
            let point = path.getPointAtLength(length);
            points.push(point);
        }

        return points;
    };

    for (let i = 1; i < segData.length; i += 1) {
        let prevSegment = segData[i - 1];
        output.push(path.getPointAtLength(prevSegment.length));
        let currSegment = segData[i];
        switch (currSegment.type) {
            case 'C':
            case 'T':
            case 'S':
            case 'Q':
            case 'A':
                let points = interpolate(prevSegment.length, currSegment.length);
                output = output.concat(points);
                break;
        }
    }

    console.dir(output);
    return output;
}

function pathToPolygon(path, samples) {
    if (!samples) samples = 0;

    // Put all path segments in a queue
    for (var segs = [], s = path.pathSegList, i = s.numberOfItems - 1; i >= 0; --i) {
        segs[i] = s.getItem(i);
    }
    var segments = segs.concat();

    var lastSeg = null, points = [], x = 0, y = 0;
    var addSegmentPoint = function (s) {
        if (s.pathSegType === SVGPathSeg.PATHSEG_CLOSEPATH) {

        } else {
            if (s.pathSegType % 2 === 1 && s.pathSegType > 1) {
                // All odd-numbered path types are relative, except PATHSEG_CLOSEPATH (1)
                x += s.x ? s.x : 0;    // if it's a relative vertical i.e. 'v' there's no horizontal coord so use 0 as the horizontal
                y += s.y ? s.y : 0;    // if it's a relative horizontal i.e. 'h' there's no vertical coord so use 0 as the vertical
            } else {
                x = s.x ? s.x : x;    // if it's a absolute vertical i.e. 'V' there's no horizontal coord so keep the horizontal where it was last time
                y = s.y ? s.y : y;    // if it's a absolute horizontal i.e. 'H' there's no vertical coord so keep the vertical where it was last time
            }
            var lastPoint = points[points.length - 1];
            if (!lastPoint || x !== lastPoint[0] || y !== lastPoint[1]) {
                points.push({
                    x, y
                });
            }
        }
    };
    let lengthPath = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    let lastLength = 0;
    for (var d = 0, len = path.getTotalLength(), step = /*len / */samples; d <= len; d += step) {
        var seg = segments[path.getPathSegAtLength(d)];

        let segmentLength = 0;

        if (seg !== lastSeg) {
            lastSeg = seg;
            while (segs.length && segs[0] !== seg) {
                let segToAdd = segs.shift();
                lengthPath.pathSegList.appendItem(segToAdd);
                let currentLength = lengthPath.getTotalLength();
                segmentLength = currentLength - lastLength;
                addSegmentPoint(segToAdd);
            }
        }

        lengthPath.pathSegList.appendItem(segs[0]);
        let currentLength = lengthPath.getTotalLength();
        segmentLength = currentLength - lastLength;
        step = segmentLength / Math.floor((segmentLength / samples));
        switch (segs[0].pathSegTypeAsLetter.toUpperCase()) {
            case 'C':
            case 'T':
            case 'S':
            case 'Q':
            case 'A':
                let pt = path.getPointAtLength(d);
                let lastPoint = points[points.length - 1];
                if (!lastPoint || pt.x !== lastPoint[0] || pt.y !== lastPoint[1]) {
                    points.push(pt);
                } else {
                    console.log("Point duplicated.");
                }
        }

    }
    for (let i = 0; i < segs.length; ++i) {
        addSegmentPoint(segs[i]);
    }
    return points;
}

function getSegmentData(path) {
    let segList = path.pathSegList;

    let segLengthList = [];
    let lastAddedLength = null;
    let lengthPath = document.createElementNS("http://www.w3.org/2000/svg", 'path');

    for (let i = 0; i < segList.numberOfItems; i += 1) {
        let segment = segList.getItem(i);
        lengthPath.pathSegList.appendItem(segment);
        let currentLength = lengthPath.getTotalLength();
        if (currentLength != lastAddedLength) {
            segLengthList.push({
                length: currentLength,
                type: segment.pathSegTypeAsLetter.toUpperCase()

            });
            lastAddedLength = currentLength;
        }
    }
    return segLengthList;
}
