# SVG Topology Analyzer

🚧 **Work in Progress** 🚧

An experimental JavaScript tool I'm building to play around with different ways of converting SVG paths into point collections. This is mostly a learning exercise and a sandbox for testing various path sampling approaches.

## Current Experiments

I'm trying out different sampling strategies to see what works best:
- Fixed distance sampling (basic but works)
- Adaptive sampling (getting better results with curves)
- Segment-aware sampling (WIP, looks promising)
- Polygon approximation (early prototype)

## Quick Start

This is pretty bare-bones right now, but if you want to try it:

1. Drop in jQuery
2. Add a canvas (`id="gc"`)
3. Include main.js
4. Put a 'track.svg' file in your directory

```html
<canvas id="gc"></canvas>
<script src="main.js"></script>
```

## Notes & Observations

The tool currently looks for paths with class 'track' in your SVG. It then attempts to:
- Load and parse the SVG
- Convert paths to points using one of the sampling methods
- Visualize the results (red dots and lines on canvas)

### Sampling Methods (all experimental)

- `pathToPoints`: The simple approach - just grab points at fixed intervals
- `pathToPointsAdaptive`: Tries to be smarter about curves
- `pathToPointsAdaptiveSegments`: Attempting to respect path segments
- `pathToPolygon`: Basic polygon approximation

### Things to Tweak

Currently playing with these values:
- `PRECISION`: Density for fixed sampling
- `tolerance`: How aggressive the adaptive sampling gets
- `minDistance`: Keeps points from clustering too close

Feel free to fork and experiment!